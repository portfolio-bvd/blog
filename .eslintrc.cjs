module.exports = {
  extends: ["plugin:astro/recommended", "eslint:recommended", "prettier"],
  plugins: ["@typescript-eslint", "prettier"],

  parserOptions: {
    sourceType: "module",
    ecmaVersion: 2018,
  },
  overrides: [
    {
      // Define the configuration for `.astro` file.
      files: ["*.astro"],
      // Allows Astro components to be parsed.
      parser: "astro-eslint-parser",
      // Parse the script in `.astro` as TypeScript by adding the following configuration.
      // It's the setting you need when using TypeScript.
      parserOptions: {
        parser: "@typescript-eslint/parser",
        extraFileExtensions: [".astro"],
      },
      rules: {
        // override/add rules settings here, such as:
        // "astro/no-set-html-directive": "error"
      },
    },
  ],
  env: {
    es6: true,
    browser: true,
    node: true,
  },
};
